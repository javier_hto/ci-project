package MingesoUsach.CIBackend.controller;

import MingesoUsach.CIBackend.model.Student;
import MingesoUsach.CIBackend.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/student")
@CrossOrigin(origins = "*")
public class StudentController {

    @Autowired
    StudentRepository studentRepository;

    @GetMapping("")
    public List<Student> index(){
        return studentRepository.findAll();
    }

    @GetMapping("/{id}")
    public Student show(@PathVariable String id){
        int studentId = Integer.parseInt(id);
        return studentRepository.findStudentById(studentId);
    }

    @PostMapping("")
    public Student create(@RequestBody Map<String, String> body) throws ParseException {
        String name = body.get("name");
        String rut = body.get("rut");
        String Sbirthdate = body.get("birthdate");
        Date birthdate = new SimpleDateFormat("yyyy-MM-dd").parse(Sbirthdate);
        return studentRepository.save(new Student(name, rut, birthdate));

    }

    @PutMapping("/{id}")
    public Student update(@PathVariable String id, @RequestBody Map<String, String> body) throws ParseException {
        int alumnoId = Integer.parseInt(id);
        //getting Student
        Student student = studentRepository.getOne(alumnoId);
        student.setName(body.get("name"));
        student.setRut(body.get("rut"));
        student.setBirthdate(new SimpleDateFormat("yyyy-MM-dd").parse(body.get("birthdate")));
        return studentRepository.save(student);
    }


    @DeleteMapping("/{id}")
    public boolean delete(@PathVariable String id){
        int studentId = Integer.parseInt(id);
        studentRepository.deleteById(studentId);
        return true;
    }

}
