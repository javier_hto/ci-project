package MingesoUsach.CIBackend.controller;

import MingesoUsach.CIBackend.model.Career;
import MingesoUsach.CIBackend.model.Postulation;
import MingesoUsach.CIBackend.model.Student;
import MingesoUsach.CIBackend.repository.CareerRepository;
import MingesoUsach.CIBackend.repository.PostulationRepository;
import MingesoUsach.CIBackend.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/postulation")
@CrossOrigin(origins = "*")
public class PostulationController {

    @Autowired
    PostulationRepository postulationRepository;

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    CareerRepository careerRepository;

    @GetMapping("")
    public List<Postulation> index(){
        return postulationRepository.findAll();
    }

    @GetMapping("/{id}")
    public Postulation show(@PathVariable String id){
        int postulationId = Integer.parseInt(id);
        return postulationRepository.findPostulationById(postulationId);
    }

    @PostMapping("")
    public Postulation create(@RequestBody Map<String, String> body) throws ParseException {
        Integer id_student = Integer.parseInt(body.get("id_student"));
        Student student = studentRepository.findStudentById(id_student);
        Integer id_career = Integer.parseInt(body.get("id_career"));
        Career career = careerRepository.findCareerById(id_career);
        return postulationRepository.save(new Postulation(student, career));
    }

    @PutMapping("/{id}")
    public Postulation update(@PathVariable String id, @RequestBody Map<String, String> body) throws ParseException {
        int postulationId = Integer.parseInt(id);
        //getting Career
        Postulation postulation = postulationRepository.getOne(postulationId);
        Integer id_student = Integer.parseInt(body.get("id_student"));
        Student student = studentRepository.findStudentById(id_student);
        Integer id_career = Integer.parseInt(body.get("id_career"));
        Career career = careerRepository.findCareerById(id_career);
        postulation.setStudent(student);
        postulation.setCareer(career);
        return postulationRepository.save(postulation);
    }


    @DeleteMapping("/{id}")
    public boolean delete(@PathVariable String id){
        int postulationId = Integer.parseInt(id);
        postulationRepository.deleteById(postulationId);
        return true;
    }

}
