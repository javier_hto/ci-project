package MingesoUsach.CIBackend.controller;

import MingesoUsach.CIBackend.model.Career;
import MingesoUsach.CIBackend.repository.CareerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/career")
@CrossOrigin(origins = "*")
public class CareerController {

    @Autowired
    CareerRepository careerRepository;

    @GetMapping("")
    public List<Career> index(){
        return careerRepository.findAll();
    }

    @GetMapping("/{id}")
    public Career show(@PathVariable String id){
        int careerId = Integer.parseInt(id);
        return careerRepository.findCareerById(careerId);
    }

    @PostMapping("")
    public Career create(@RequestBody Map<String, String> body) throws ParseException {
        String cod = body.get("cod");
        String name = body.get("name");
        return careerRepository.save(new Career(cod, name));
    }

    @PutMapping("/{id}")
    public Career update(@PathVariable String id, @RequestBody Map<String, String> body) throws ParseException {
        int careerId = Integer.parseInt(id);
        //getting Career
        Career career = careerRepository.getOne(careerId);
        career.setCod(body.get("cod"));
        career.setName(body.get("name"));
        return careerRepository.save(career);
    }


    @DeleteMapping("/{id}")
    public boolean delete(@PathVariable String id){
        int careerId = Integer.parseInt(id);
        careerRepository.deleteById(careerId);
        return true;
    }

}
