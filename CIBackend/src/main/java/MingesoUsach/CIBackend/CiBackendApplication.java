package MingesoUsach.CIBackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CiBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(CiBackendApplication.class, args);
	}

}
