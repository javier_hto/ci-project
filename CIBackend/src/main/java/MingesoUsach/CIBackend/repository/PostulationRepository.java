package MingesoUsach.CIBackend.repository;

import MingesoUsach.CIBackend.model.Postulation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostulationRepository extends JpaRepository<Postulation, Integer>{
    //Custom querys
     Postulation findPostulationById(Integer id);
}
