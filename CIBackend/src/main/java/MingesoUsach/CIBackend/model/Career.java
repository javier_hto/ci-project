package MingesoUsach.CIBackend.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "career")
public class Career implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "cod")
    private String cod;
    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "career", cascade = CascadeType.ALL)
    private Set<Postulation> postulations;

    public Career(){}

    public Career(String cod, String name)
    {
        this.cod = cod;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
