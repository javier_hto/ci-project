package MingesoUsach.CIBackend.model;

import jdk.jfr.DataAmount;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;


@Entity
@Table(name = "student")
public class Student implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;
    @Column(name = "rut")
    private String rut;
    @Column(name = "birthdate")
    private Date birthdate;

    @OneToMany(mappedBy = "student", cascade = CascadeType.ALL)
    private Set<Postulation> postulations;

    public Student(){}

    public Student(String name, String rut, Date birthdate){
        this.name = name;
        this.rut = rut;
        this.birthdate = birthdate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    @Override
    public String toString(){
        return "Alumno{" +
                "id=" + this.id +
                ", name='" + this.name + '\'' +
                ", rut=" + rut + '\'' +
                ", birthdate=" + this.birthdate +
                '}';
    }
}
