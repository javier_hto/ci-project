package MingesoUsach.CIBackend.model;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "postulation")
public class Postulation implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name="id_student")
    private Student student;

    @ManyToOne
    @JoinColumn(name="id_career")
    private Career career;

    public Postulation(){}

    public Postulation(Student student, Career career){
        this.student = student;
        this.career = career;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Career getCareer() {
        return career;
    }

    public void setCareer(Career career) {
        this.career = career;
    }
}
