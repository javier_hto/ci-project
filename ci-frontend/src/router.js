import Vue from 'vue'
import Router from 'vue-router'
import Home from "./views/Home.vue"
import New from "./views/New.vue"
import ListStudents from "@/components/ListStudents.vue"


Vue.use(Router)
export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },

    {
      path: "/new",
      name: "new",
      component: New
    },

    {
      path: "/ListStudents",
      name: "ListStudents",
      component: ListStudents
    },
  ]
});
